<?php

/**
 * @file
 */

namespace Drupal\simple_subscription;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an aggregator feed entity.
 */
interface SubscriptionInterface extends ContentEntityInterface {

  /**
   * Returns the mail to the subscription.
   *
   * @return string
   *   The mail to the subscription.
   */
  public function getMail();

  /**
   * Sets the mail to the subscription.
   *
   * @param string $mail
   *   A string containing the mail of the subscription.
   *
   * @return \Drupal\simple_subscription\SubscriptionInterface
   *   The class instance that this method is called on.
   */
  public function setMail($mail);

  /**
   * Returns the calculated hash of the subscription data, used for validating cache.
   *
   * @return string
   *   The calculated hash of the subscription data.
   */
  public function getHash();

  /**
   * Sets the calculated hash of the subscription data, used for validating cache.
   *
   * @param string $hash
   *   A string containing the calculated hash of the subscription. Must contain
   *   US ASCII characters only.
   *
   * @return \Drupal\simple_subscription\SubscriptionInterface
   *   The class instance that this method is called on.
   */
  public function setHash($hash);

  /**
   * Returns the status of the subscription data.
   *
   * @return int
   *   The status of subscription data.
   */
  public function getStatus();

  /**
   * Sets the status of subscription data.
   *
   * @param int $status
   *   A int containing status of the subscription.
   *
   * @return \Drupal\simple_subscription\SubscriptionInterface
   *   The class instance that this method is called on.
   */
  public function setStatus($status);

  /**
   * Returns the uid of subscription data.
   *
   * @return string
   *   The uid of the subscription data.
   */
  public function getUid();

  /**
   * Sets the uid of subscription data.
   *
   * @param int $uid
   *   A int containing uid of subscription data.
   *
   * @return \Drupal\simple_subscription\SubscriptionInterface
   *   The class instance that this method is called on.
   */
  public function setUid($uid);

  /**
   * Returns the created date of subscription data.
   *
   * @return string
   *   The uid of the subscription data.
   */
  public function getCreated();

  /**
   * Sets the uid of subscription data.
   *
   * @param int $created
   *   A int containing creation timestamp of subscription data.
   *
   * @return \Drupal\simple_subscription\SubscriptionInterface
   *   The class instance that this method is called on.
   */
  public function setCreated($created);

  /**
   * Sets the time of subscription data.
   *
   * @param int $deleted
   *   A int containing deletion timestamp of subscription data.
   *
   * @return \Drupal\simple_subscription\SubscriptionInterface
   *   The class instance that this method is called on.
   */
  public function setDeleted($deleted);

}
