<?php
/**
 * @file
 * Contains \Drupal\simple_subscription\Controller\SimpleSubscriptionController.
 */
namespace Drupal\simple_subscription\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_subscription\Entity\Subscription;
use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Entity\EntityTypeManager;
/**
 *
 */
class SimpleSubscriptionController extends ControllerBase {

  protected $entityQuery, $logger, $conn, $entityManager;
  /**
   *
   */
  public function __construct(Connection $conn, EntityTypeManager $entityManager) {

    $this->entityManager = $entityManager;
    $this->conn = $conn;
  }

  /**
   *
   */
  public function unsubscribePage($subscription) {

    $subscription_status = $subscription->getStatus();
    if (!$subscription_status) {
      drupal_set_message($this->t("You have already unsubscribed from our newsletter."));
    }
    else {
      $subscription->setStatus(FALSE);
      $subscription->setDeleted(time());
      $subscription->save();
      drupal_set_message($this->t("You were successfully removed from the newsletter recipients."));
    }
    $url = Url::fromRoute('<front>', []);
    $response = new RedirectResponse($url->toString());
    $response->send();
  }

  /**
   *
   */
  public function unsubscribeHash($subscription_hash) {
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   *
   */
  public function unsubscribe($sids) {
    $subscriptions = Subscription::loadMultiple($sids);
    foreach ($subscriptions as $subscription) {
      $this->unsubscribePage($subscription);
    }
  }

}
