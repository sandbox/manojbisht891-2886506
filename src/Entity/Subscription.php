<?php

/**
 * @file
 */

namespace Drupal\simple_subscription\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\simple_subscription\SubscriptionInterface;

/**
 * Defines the Simple Subscription entity.
 *
 * @ingroup subscription
 *
 * @ContentEntityType(
 *   id = "Subscription",
 *   label = @Translation("Subscription"),
 *   base_table = "simple_subscription_subscriptions",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "sid",
 *   },
 * )
 */
class Subscription extends ContentEntityBase implements SubscriptionInterface {

  /**
   *
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['sid'] = BaseFieldDefinition::create('integer')
                      ->setLabel(t('sid'))
                      ->setDescription(t('The primary identifier for a subscription.'))
                      ->setReadOnly(TRUE);

    $fields['mail'] = BaseFieldDefinition::create('string')
                      ->setLabel(t('mail'))
                      ->setDescription(t('The e-mail address of the subscription.'))
                      ->setSettings(array(
                        'default_value' => '',
                        'max_length' => 255,
                        'text_processing' => 0,
                      ));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
                      ->setLabel(t('uid'))
                      ->setDescription(t('The {users}.uid that owns this node; initially, this is the user that created it.'))
                      ->setSetting('target_type', 'user');

    $fields['hash'] = BaseFieldDefinition::create('string')
                      ->setLabel(t('hash'))
                      ->setDescription(t('The hash related to the subscription.'))
                      ->setSettings(array(
                        'default_value' => '',
                        'max_length' => 255,
                        'text_processing' => 0,
                      ));

    $fields['status'] = BaseFieldDefinition::create('boolean')
                      ->setLabel(t('status'))
                      ->setDescription(t('Boolean indicating whether the subscription is active.'))
                      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
                      ->setLabel(t('created'))
                      ->setDescription(t('The Unix timestamp when the user created his subscription.'));

    $fields['deleted'] = BaseFieldDefinition::create('changed')
                      ->setLabel(t('deleted'))
                      ->setDescription(t('The Unix timestamp when the user deleted his subscription.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getUid() {
    return $this->get('uid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUid($uid) {
    $this->set('uid', $uid);
  }

  /**
   * {@inheritdoc}
   */
  public function getHash() {
    return $this->get('hash')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHash($hash) {
    $this->set('hash', $hash);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
  }

  /**
   * {@inheritdoc}
   */
  public function getMail() {
    return $this->get('mail')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMail($mail) {
    $this->set('mail', $mail);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated($created) {
    $this->set('created', $created);
  }

  /**
   * {@inheritdoc}
   */
  public function setDeleted($deleted) {
    $this->set('deleted', $deleted);
  }

  /**
   * {@inheritdoc}
   */
  public function save() {

    $related_entity = \Drupal::entityTypeManager()->getStorage('Subscription');
    return $related_entity->save($this);
  }

}
