<?php

/**
 * @file
 * Contains \Drupal\simple_subscription\Form\SimpleSubscriptionAdminForm.
 */

namespace Drupal\simple_subscription\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_subscription\SimpleSubscriptionService;
/**
 *
 */
class SimpleSubscriptionAdminForm extends FormBase {

  protected $simple_subscription;
  /**
   *
   */
  public function __construct(SimpleSubscriptionService $simple_subscription) {

    $this->simple_subscription = $simple_subscription;
  }

  /**
   *
   */
  public function getFormId() {
    return 'simple_subscription_admin_form';
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $header = array(
      'mail' => array('data' => 'E-mail', 'field' => 'mail', 'sort' => 'asc'),
      'uid' => array('data' => 'Account', 'field' => 'uid'),
      'status' => array('data' => 'Status', 'field' => 'status'),
      'created' => array('data' => 'Created', 'field' => 'created'),
    );
    $results = $this->simple_subscription->fetch_data();
    // Prepare the list of subscribers.
    $options = array();

    foreach ($results as $result) {
      $options[$result->sid] = array(
        'mail' => $result->mail,
        'uid' => $result->uid ? 'user/' . $result->uid : '',
        'status' => $result->status ? 'Active' : 'Inactive',
        'created' => format_date($result->created, 'short'),
      );
    }

    // Prepare form elements.
    $form['subscriptions'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );

    $form['submit'][] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );

    return $form;
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $subscriptions = $form_state->getValue('subscriptions');
    if (!is_array($subscriptions) || !count(array_filter($subscriptions))) {
      $form_state->setErrorByName('subscriptions', t('No Items Selected.'));
    }
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sids = array_filter($form_state->getValue('subscriptions'));
    $this->simple_subscription->delete($sids);
    $count = count($sids);
    drupal_set_message(t('@count subscription deleted.', array('@count' => $count)));
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_subscription.service')
    );
  }

}
