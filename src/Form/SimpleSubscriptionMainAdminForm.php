<?php

/**
 * @file
 * Contains \Drupal\simple_subscription\Form\SimpleSubscriptionMainAdminForm.
 */

namespace Drupal\simple_subscription\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
/**
 *
 */
class SimpleSubscriptionMainAdminForm extends ConfigFormBase {

  /**
   *
   */
  public function getFormId() {
    return 'simple_subscription_main_admin_form';
  }

  /**
   *
   */
  protected function getEditableConfigNames() {
    return [
      'simple_subscription.settings',
    ];
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('simple_subscription.settings');

    if (empty($config->get('ssubscription_user_account_attach'))) {
      $ssubscription_user_account_attach = FALSE;
    }
    else {
      $ssubscription_user_account_attach = TRUE;
    }

    if (empty($config->get('ssubscription_user_account_add'))) {
      $ssubscription_user_account_add = FALSE;
    }
    else {
      $ssubscription_user_account_add = TRUE;
    }

    $form['ssubscription_user_account_attach'] = array(
      '#type'         => 'checkbox',
      '#title'        => t('Attach subscriptions to user accounts.'),
      '#description'  => t('If a logged in user subscribes to the newsletter(s), then he can manage subscriptions from his user account, and no verification mail is sent on subscription. Only one subscription mail is allowed by Drupal user.<br />If unchecked no correspondance will be kept between subscriptions and user accounts.'),
      '#default_value' => $ssubscription_user_account_attach,
    );

    $form['ssubscription_user_account_attach_options'] = array(
      '#type'  => 'fieldset',
      '#title' => t('Drupal users subscription options'),
      '#states' => array(
        'visible' => array(
          ':input[name="ssubscription_user_account_attach"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['ssubscription_user_account_attach_options']['ssubscription_user_account_add'] = array(
      '#type'   => 'checkbox',
      '#title'  => t('Add subscription form to user edit form'),
      '#default_value' => $ssubscription_user_account_add,
    );

    $form['ssubscription_user_account_attach_options']['ssubscription_delete_user_action'] = array(
      '#type'    => 'radios',
      '#title'   => t('On user delete'),
      '#options' => array(
        SSUBSCRIPTION_DELETE_USER_NOTHING  => t('Do nothing.'),
        SSUBSCRIPTION_DELETE_USER_UNSUBSCRIBE => t('Remove subscription (but keep record).'),
        SSUBSCRIPTION_DELETE_USER_DELETE   => T('Delete subscription.'),
      ),
      '#default_value' => $config->get('ssubscription_delete_user_action')?:SSUBSCRIPTION_DELETE_USER_NOTHING,
      '#description' => t('Choose action to perform when a subscribed user is deleted from Drupal'),
    );

    $form['ssubscription_user_account_attach_options']['ssubscription_block_user_options'] = array(
      '#type'    => 'radios',
      '#title'   => t('Subscription block state when the current user is already subscribed'),
      '#options' => array(
        SSUBSCRIPTION_BLOCK_VISIBILITY_HIDE    => t('Hide block for subscribed users.'),
        SSUBSCRIPTION_BLOCK_VISIBILITY_PREFILL => t('Prefill block with current subscription options.'),
      ),
      '#default_value' => $config->get('ssubscription_block_user_options')?:SSUBSCRIPTION_BLOCK_VISIBILITY_HIDE,
      '#description' => t('Choose action to perform when a subscribed user is deleted from Drupal'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $input = $form_state->getUserInput();
    foreach ($input as $input_key => $input_val) {
      if (preg_match("/^ss/i", $input_key)) {
        $this->config('simple_subscription.settings')
            ->set($input_key, $input_val)
            ->save();
      }
    }
    parent::submitForm($form, $form_state);
  }

}
