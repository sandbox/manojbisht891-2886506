<?php

/**
 * @file
 * Contains \Drupal\simple_subscription\Form\SimpleSubscriptionExportForm.
 */

namespace Drupal\simple_subscription\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_subscription\SimpleSubscriptionService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_subscription\Entity\Subscription;
use Drupal\Core\Datetime\DateFormatter;
use Symfony\Component\HttpFoundation\Response;
/**
 *
 */
class SimpleSubscriptionExportForm extends FormBase {

  protected $simple_subscription, $dateFormatter;
  /**
   *
   */
  public function __construct(SimpleSubscriptionService $simple_subscription, DateFormatter $dateFormatter) {

    $this->simple_subscription = $simple_subscription;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   *
   */
  public function getFormId() {
    return 'simple_subscription_export_form';
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#prefix'] = t('<p>This form allows you to export subscriptions in a CSV (Comma-Separated Value) file. However, you can define which separator you want to use.</p>');

    $form['delimiter'] = array(
      '#type' => 'select',
      '#title' => t('Delimiter'),
      '#options' => array(
        1 => ',',
        2 => ';',
        3 => 'tab',
      ),
      '#default_value' => ',',
    );

    // TODO: implement more fields.
    $form['headers'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Headers'),
      '#options' => array(
        'mail' => t('E-mail address'),
        'created' => t('Date of subscription'),
      ),
      '#default_value' => array('mail'),
      '#required' => TRUE,
      '#description' => t('Define which columns you want to appear in the CSV file.'),
    );

    $form['skip'] = array(
      '#type' => 'checkbox',
      '#title' => t('Skip first row'),
      '#description' => t('If enabled, no headers will be added to the file.'),
    );

    $form['quote'] = array(
      '#type' => 'checkbox',
      '#title' => t('Force quote wrapping'),
      '#description' => t('If enabled, values will always be wrapped between quotes.'),
      '#default_value' => TRUE,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Export'),
    );

    return $form;
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $subscriptions = Subscription::loadMultiple(NULL);
    if (empty($subscriptions)) {
      $form_state->setErrorByName('', t('There are no subscriptions to export.'));
    }
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $output = '';
    // Build the query.
    $fields = array_filter($form_state->getValue('headers'));
    $results = $this->simple_subscription->fetch_export_data($fields);
    $delimiters = array(
      1 => ',',
      2 => ';',
      3 => 'tab',
    );
    $del = $delimiters[$form_state->getValue('delimiter')];
    // Append table headers.
    if ($form_state->getValue('skip') == FALSE) {
      foreach ($fields as $field) {

        $quote = $form_state->getValue('quote');
        $untranslated_string = $form['headers']['#options'][$field]->getUntranslatedString();
        if ($quote) {
          $output .= $untranslated_string . $del;
        }
        else {
          $output .= '"' . addcslashes($untranslated_string, '"') . '"' . $del;
        }
      }
      $output .= "\n";
    }

    foreach ($results as $result) {
      foreach ($fields as $field) {
        $value = $this->formatValue($field, $result->{$field});
        $quote = $form_state->getValue('quote');
        if ($quote && strstr($value, '"') == FALSE) {
          $output .= $value . $del;
        }
        else {
          $output .= '"' . addcslashes($value, '"') . '"' . $del;
        }
      }
      $output .= "\n";
    }

    $response = new Response();
    $response->setContent($output);
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Cache-Control', 'no-store, no-cache');
    $response->headers->set('Content-Disposition', 'attachment; filename="' . t('subscriptions') . '.csv');
    $response->send();
    exit;
  }

  /**
   *
   */
  public function formatValue($field_name, $value) {
    switch ($field_name) {
      case 'mail':
        return $value;

      break;
      case 'created':
        return $this->dateFormatter->format($value, 'custom', 'd/m/Y H:i:s');

      break;
    }
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_subscription.service'),
      $container->get('date.formatter')
    );
  }

}
