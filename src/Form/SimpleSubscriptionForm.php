<?php

/**
 * @file
 * Contains \Drupal\simple_subscription\Form\SimpleSubscriptionForm.
 */

 namespace Drupal\simple_subscription\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_subscription\SimpleSubscriptionService;
/**
 *
 */
class SimpleSubscriptionForm extends FormBase {

  protected $subscription;
  /**
   *
   */
  public function __construct(SimpleSubscriptionService $subscription) {
    $this->subscription = $subscription;
  }

  /**
   *
   */
  public function getFormId() {
    return 'simple_subscription_form';
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['header'] = array(
      '#prefix'   => '<div class="simple_subscription_header">',
      '#markup'  => '',
      '#suffix'   => '</div>',
    );

    $form['mail'] = array(
      '#type'           => 'textfield',
      '#maxlength'      => 255,
      '#size'           => '',
      '#required'       => TRUE,
      '#attributes'     => array('class' => array('edit-mail')),
    );

    $form['footer'] = array(
      '#prefix'   => '<div class="simple_subscription_footer">',
      '#markup'  => '',
      '#suffix'   => '</div>',
    );

    $form['submit'] = array(
      '#type'  => 'submit',
      '#value' => '',
    );

    return $form;
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $save = FALSE;
    $mail = $form_state->getValue('mail');
    $subscription = $this->subscription->loadSubscription(array('mail' => $mail));;
    if (!empty($subscription)) {
      if ($subscription->getStatus() == 0) {
        $subscription->setStatus(1);
        $subscription->setCreated(time());
        $msg = t('Welcome back to our newsletter.');
        $save = TRUE;
        $subscription->save();
      }
      else {
        $msg = t('You are already subscribed to our newsletter.');
      }
    }
    else {
      $subscription = new \stdClass();
      $subscription->mail = $mail;
      $subscription->status = 1;
      $subscription->created = time();
      $this->subscription->save($subscription);
      $msg = t('Subscription has been added.');
      $save = TRUE;
    }

    if (!empty($subscription)) {
      drupal_set_message($msg);
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
     // Load the service required to construct this class.
     $container->get('simple_subscription.service')
    );
  }

}
