<?php

/**
 * @file
 */

namespace Drupal\simple_subscription;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\simple_subscription\Entity\Subscription;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
/**
 *
 */
class SimpleSubscriptionService {

  protected $conn, $entity_query, $loggerFactory, $configFactory, $entityManager;
  /**
   *
   */
  public function __construct(Connection $conn, QueryFactory $entity_query, LoggerChannelFactoryInterface $loggerFactory, ConfigFactoryInterface $configFactory, EntityTypeManager $entityManager) {

    $this->conn = $conn;
    $this->entity_query = $entity_query;
    $this->loggerFactory = $loggerFactory;
    $this->configFactory = $configFactory;
    $this->entityManager = $entityManager;
  }

  /**
   *
   */
  public function get() {
    return $this;
  }

  /**
   *
   */
  public function fetch_data() {

    $query = $this->conn->select('simple_subscription_subscriptions', 's');
    $query->fields('s');
    $query->leftJoin('users_field_data', 'u', 's.uid = u.uid');
    $query = $query->fields('u', array('name'));
    $results = $query->execute()->fetchAll();
    return $results;
  }

  /**
   *
   */
  public function fetch_export_data($fields) {
    $query = $this->conn->select('simple_subscription_subscriptions', 's');
    $query->fields('s', $fields);
    $results = $query->execute()->fetchAll();
    return $results;
  }

  /**
   *
   */
  public function loadSubscription($cond) {
    $query = $this->entity_query->get('Subscription');
    if (!empty($cond)) {
      foreach ($cond as $cond_key => $cond_val) {
        $query->condition($cond_key, $cond_val);
      }
    }
    $sid_array = $query->execute();
    if (!empty($sid_array)) {
      $sid = array_values($sid_array)[0];
      return Subscription::load($sid);
    }
    else {
      return '';
    }

  }


  /**
   *
   */
  public function save($subscription) {

    $user = \Drupal::currentUser();
    $op = isset($subscription->sid) ? 'insert' : 'update';
    $config = $this->configFactory->get('simple_subscription.settings');
    if (!isset($subscription->sid)) {
      if (!isset($subscription->mail) && empty($subscription->mail)) {
        return FALSE;
      }
      if (!isset($subscription->created) || empty($subscription->created)) {
        $subscription->created = time();
      }
      if (!isset($subscription->hash) || empty($subscription->hash)) {
        $subscription_hash = simple_subscription_generate_hash();
        $subscription->hash = $subscription_hash;
      }
      if (empty($config->get('ssubscription_user_account_attach'))) {
        $subscription->uid = 0;
      }
      else {
        if (empty($subscription->uid)) {
          $subscription->uid = $user->id();
        }
      }
      if (!isset($subscription->status) || empty($subscription->status)) {
        $subscription->status = 1;
      }

      $subscription = Subscription::create([
        'mail' => $subscription->mail,
        'created' => $subscription->created,
        'hash' => $subscription->hash,
        'uid' => $subscription->hash,
        'status' => $subscription->status,
      ]);

    }
    else {
      $subscription = Subscription::load(1);
      // $subscription = Subscription::load(1);.
      if (empty($subscription->getMail())) {
        return FALSE;
      }

      if (empty($subscription->getCreated())) {
        $subscription->setCreated(time());
      }

      if (empty($subscription->getHash())) {
        $subscription_hash = simple_subscription_generate_hash();
        $subscription->setHash($subscription_hash);
      }
      // $config = $this->config('simple_subscription.settings');.
      if (empty($config->get('ssubscription_user_account_attach'))) {
        $subscription->setUid(0);
      }
      else {
        if (empty($subscription->getUid())) {
          $subscription->setUid($user->id());
        }
      }
    }
    $subscription_save = $subscription->save();

    if ($subscription_save && ($op == 'insert')) {
      $this->loggerFactory->get('simple_subscription')->notice("New subscription added (@submitted_email).", array("@submitted_email" => $subscription->getMail()));
    }
    elseif ($subscription_save && ($op == 'update')) {
      $this->loggerFactory->get('simple_subscription')->notice("Subscription updated (@submitted_email)", array("@submitted_email" => $subscription->getMail()));
    }
    else {
      $this->loggerFactory->get('simple_subscription')->error("An error occurred while adding @submitted_email", array("@submitted_email" => $subscription->getMail()));
    }
    /** Has to be implemented later. **/
    // invokeAll('subscription_' . $op, $subscription);.
    return $subscription_save ? $subscription : FALSE;
  }

  /**
   *
   */
  public function delete($sids) {
    $subscriptions = Subscription::loadMultiple($sids);
    foreach ($subscriptions as $subscription) {
      // invokeAll('subscription_delete', $subscription);.
    }
    // simple_subscription_delete_multiple($sids);
    $query = $this->conn->delete('simple_subscription_subscriptions');
    $query->condition('sid', $sids, 'IN');
    $query->execute();
    $this->entityManager->getStorage('Subscription')->resetCache($sids);
  }

}
