<?php
/**
 * @file
 * Contais Drupal\simple_subscription\Plugin\Block\SimpleSubscriptionBlock.
 */
namespace Drupal\simple_subscription\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Utility\Token;

/**
 * Provides a Simple Subscription Block.
 *
 * @Block(
 *   id = "simple_subscription_block",
 *   admin_label = @Translation("Simple Subscription Block"),
 *   category = @Translation("Simple Subscription Block")
 * )
 */
class SimpleSubscriptionBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  protected $module_handler, $form_builder, $config, $token;
  /**
   *
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, ModuleHandler $module_handler, FormBuilder $form_builder, ConfigFactory $config, Token $token) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->module_handler = $module_handler;
    $this->form_builder = $form_builder;
    $this->config = $config;
    $this->token = $token;
  }

  /**
   *
   */
  public function build() {
    $config = $this->getConfiguration();

    $form = $this->form_builder->getForm('Drupal\simple_subscription\Form\SimpleSubscriptionForm');

    $user_account_attach = $this->config->get('simple_subscription.settings')->get('ssubscription_user_account_attach');
    $user_block_user_options = $this->config->get('simple_subscription.settings')->get('ssubscription_block_user_options');

    if ($user_account_attach && $user_block_user_options) {
      print 'Inside 1'; echo '<br />';
      $form['mail']['#default_value'] = 'manoj.bisht@qed42.com';
    }
    else {
      print 'Inside 2'; echo '<br />';
      $form['mail']['#default_value'] = 'm.bisht33@gmail.com';
    }

    if (!empty($config['simple_subscription_form_header'])) {
      $form['header']['#markup'] = $this->token->replace(Xss::filterAdmin($config['simple_subscription_form_header']));
    }

    $form['mail']['#size'] = $config['simple_subscription_input_size'];

    if (!empty($config['simple_subscription_input_label'])) {
      $form['mail']['#title'] = Html::escape($config['simple_subscription_input_label']);
    }

    $input_content = Html::escape($config['simple_subscription_input_label']);
    // If (!empty($input_content)) {
    //   // $form['mail']['#attached']['js'][] = drupal_get_path('module', 'simple_subscription') . '/simple_subscription.js';
    //   // $form['mail']['#attached']['js'][] = array(
    //   //   'data'  => array(
    //   //     'simple_subscription' => array('input_content' => ($input_content))
    //   //   ),
    //   //   'type'  => 'setting',
    //   // );
    //   // $form['mail']['#attached']['css'][] = drupal_get_path('module', 'simple_subscription') . '/simple_subscription.css';
    // }.
    if (!empty($config['simple_subscription_form_footer'])) {
      $form['footer']['#markup'] = $this->token->replace(Xss::filterAdmin($config['simple_subscription_form_footer']));
    }

    $form['submit']['#value'] = $this->token->replace(Html::escape($config['simple_subscription_submit_value']));

    return $form;

  }

  /**
   *
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['city_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('City Name'),
      '#description' => $this->t('Enter name of city for which you want to get weather'),
      '#default_value' => isset($config['city_name']) ? $config['city_name'] : '',
    );

    $form['simple_subscription_form_header'] = array(
      '#type'          => 'textarea',
      '#title'         => t('Form header'),
      '#default_value' => isset($config['simple_subscription_form_header']) ? Xss::filterAdmin($config['simple_subscription_form_header']) : '',
      '#description'   => t('This text will be displayed before the form elements (leave empty for none). You may use Html tags.'),
    );

    $form['simple_subscription_form_footer'] = array(
      '#type'          => 'textarea',
      '#title'         => t('Form footer'),
      '#default_value' => isset($config['simple_subscription_form_footer']) ? Xss::filterAdmin($config['simple_subscription_form_footer']) : '',
      '#description'   => t('This text will be displayed after the form elements (leave empty for none). You may use Html tags.'),
    );

    $form['token_help'] = array(
      '#title'       => t('Replacement patterns for the text fields above'),
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );

    if ($this->module_handler->moduleExists('token')) {
      $form['token_help']['token_tree'] = array(
        '#theme'        => 'token_tree_link',
        '#token_types' => array(),
      );
    }
    else {
      $form['token_help']['install_token'] = array(
        '#markup' => t('<p>Install the <a href="http://drupal.org/project/token" title="Token module project page">Token module</a> to see a list of the available tokens</p>'),
      );
    }

    $form['simple_subscription_input_label'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Input field label'),
      '#size'          => 20,
      '#default_value' => isset($config['simple_subscription_input_label']) ? Html::escape($config['simple_subscription_input_label']) : '',
      '#description'   => t('A label for the e-mail input field (Leave empty for none).'),
    );

    $form['simple_subscription_input_size'] = array(
      '#type'          => 'select',
      '#title'         => t('Input field size'),
      '#default_value' => isset($config['simple_subscription_input_size']) ? $config['simple_subscription_input_size'] : '',
      '#description'   => t('Html size attribute for the e-mail input field.'),
      '#options'       => array(
        15  => '15',
        20  => '20',
        25  => '25',
        30  => '30',
        35  => '35',
        40  => '40',
        45  => '45',
        50  => '50',
      ),
    );

    $form['simple_subscription_input_content'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Input field text'),
      '#size'          => 20,
      '#default_value' => isset($config['simple_subscription_input_content']) ? Html::escape($config['simple_subscription_input_content']) : '',
      '#description'   => t('A short string to display inside the input field. This text is added by javascript and will only be visible if the field is empty and not focused (only on browsers with javascript activated). Be careful not to repeat the information you added in the field label above.'),
    );

    $form['simple_subscription_submit_value'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Submit button value'),
      '#size'          => 20,
      '#default_value' => isset($config['simple_subscription_submit_value']) ? Html::escape($config['simple_subscription_submit_value']) : '',
      '#description'   => t('Contents for the form submit button. Cannot be empty and defaults to <em>@default_value</em>. No tokens are allowed.', array('@default_value' => $config['simple_subscription_submit_value'])),
    );

    $form['simple_subscription_success_message'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Success message'),
      '#size'          => 60,
      '#default_value' => isset($config['simple_subscription_success_message']) ? Xss::filterAdmin($config['simple_subscription_success_message']) : '',
      '#description'   => t('The message to display on successful e-mail submission (leave empty for none). You may use Html tags and you may use the aditionnal <em>[simple-subscription:email]</em> token.'),
    );

    $form['simple_subscription_redirect_path'] = array(
      '#type'             => 'textfield',
      '#title'            => t('Redirect path'),
      '#size'             => 60,
      '#default_value'    => isset($config['simple_subscription_redirect_path']) ? Html::escape($config['simple_subscription_redirect_path']) : '',
      '#description'      => t("The internal path to redirect the user to after a successful login. Use '/' for the front page, leave empty to stay on the same page."),
    );

    return $form;
  }

  /**
   *
   */
  public function blockValidate($form, FormStateInterface $form_state) {
  }

  /**
   *
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $form_state_values = $form_state->getValues();
    foreach ($form_state_values as $form_id => $form_val) {
      $this->configuration[$form_id] = $form_val;
    }
  }

  /**
   *
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('form_builder'),
      $container->get('config.factory'),
      $container->get('token')
    );
  }

}
