<?php
/**
 * @file
 * Views 3 support for Simple subscription.
 */
function simple_subscription_views_data() {

  // ----------------------------------------------------------------
  // imple subscription table -- basic table information.
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['simple_subscription_subscriptions']['table']['group'] = t('Subscriptions');
  $data['simple_subscription_subscriptions']['table']['provider'] = 'simple_subscription';

  // Advertise this table as a possible base table.
  $data['simple_subscription_subscriptions']['table']['base'] = array(
    'field' => 'sid',
    'title' => t('Subscription'),
    'help' => t("Subscriptions are information about newsletter's subscribers."),
    'weight' => -10,
  );

  $data['simple_subscription_subscriptions']['table']['join'] = array(
    // Subscription links to users via uid.
    'users_field_data' => array(
      'left_table' => 'simple_subscription_subscriptions',
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  // ----------------------------------------------------------------
  // simple_subscription table -- fields.
  // Sid.
  $data['simple_subscription_subscriptions']['sid'] = array(
    'title' => t('Sid'),
  // The help that appears on the UI,.
    'help' => t('The subscription ID of the subscription.'),
    // Information for displaying the sid.
    'field' => array(
      'id' => 'standard',
    ),
    // Information for accepting a sid as a filter.
    'filter' => array(
      'id' => 'numeric',
    ),
    'argument' => array(
      'id' => 'numeric',
    ),
    // Information for sorting on a sid.
    'sort' => array(
      'id' => 'standard',
    ),
  );

  $data['simple_subscription_subscriptions']['mail'] = array(
    'title' => t('E-mail'),
    'help' => t('The e-mail address of the subscriber.'),
    'field' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  $data['simple_subscription_subscriptions']['hash'] = array(
    'title' => t('Hash'),
    'help' => t('The hash used in mail for this subscription.'),
    'filter' => array(
      'id' => 'string',
    ),
  );

  $data['users_field_data']['table']['join']['simple_subscription_subscriptions'] = array(
    'left_table' => 'users_field_data',
    'left_field' => 'uid',
    'field' => 'uid',
  );

  return $data;
}
