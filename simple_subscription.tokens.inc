<?php

/**
 * @file
 * Builds placeholder replacement tokens for comment-related data.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function simple_subscription_token_info() {
  $tokens = array(
    'types'  => array(
      'simple-subscription' => array(
        'name'         => 'Simple subscription',
        'description'  => t('Simple subscription form data'),
        'needs-data'   => 'simple-subscription',
      ),
    ),
    'tokens' => array(
      'simple-subscription' => array(
        'sid' => array(
          'name'  => t('Subscription id'),
          'description' => t('The subscription ID.'),
        ),
        'email' => array(
          'name'  => t('Submitted email address'),
          'description' => t('Email address submitted using the <em>Simple subscription</em> block.'),
        ),
        'unsubscribe-link' => array(
          'name' => t('Unsubscribe link'),
          'description' => t('Link to unsubscribe with the subscription hashed.'),
        ),
      ),
    ),
  );

  return $tokens;
}

/**
 * Implements hook_tokens().
 */
function simple_subscription_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $token_service = \Drupal::token();

  $return = array();
  if (($type != 'simple-subscription') || (!isset($data['simple-subscription']))) {
    return $return;
  }

  $simple_subscription = $data['simple-subscription'];

  foreach ($tokens as $machine_name => $raw_text) {
    switch ($machine_name) {
      case 'sid':
        if (isset($data['simple-subscription']['sid'])) {
          $return[$raw_text] = $data['simple-subscription']['sid'];
        }
        break;

      case 'email':
        if (isset($data['simple-subscription']['mail'])) {
          $return[$raw_text] = $data['simple-subscription']['mail'];
        }
        break;

      case 'unsubscribe-link':
        if (isset($data['simple-subscription']['hash'])) {
          $url = 'newsletter/unsubscribe-hash/' . $data['simple-subscription']['hash'];
          $return[$raw_text] = l(t('Unsubscribe'), $url, array(
            'absolute' => TRUE,
          ));
        }
        break;

      default:
        break;
    }
  }

  return $return;
}
